import { ctx } from "./canvas";
import Circle from "./class/Circle";
import { getDistance } from "./utils";



const mouse = {
    x: 10,
    y: 10
  }

const circle1 = new Circle({x:window.innerWidth / 2, y:window.innerHeight / 2,radius:100, color:'#0C356A'})
const circle2 = new Circle({x:0,y:0,radius:30, color:'#00A9FF'})

function animate(){
    requestAnimationFrame(animate)
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    circle1.update()

    circle2.x = mouse.x 
    circle2.y = mouse.y
    circle2.update()

    const distance = getDistance(circle1.x, circle1.y, circle2.x, circle2.y)
    if(distance < (circle1.radius + circle2.radius)){
        circle1.color = '#0C356A'
    }
    else{
        circle1.color = '#FFC436'
    }
}

animate()


addEventListener('mousemove',e=>{
    mouse.x = e.clientX
    mouse.y = e.clientY
})


